function openNav(value) { 
	var cmdAction = value;
	var elem = document.getElementById("mySidenav");
	
	if(typeof elem !== 'undefined' && elem !== null) {
		document.getElementById("mySidenav").style.width = "100%";
		
		if (cmdAction !== null) {
			$('#pageContent').empty();
			
			if (cmdAction == 'Savings') { 
				$('#pageContent').load('../pages/register.html');
			} else if (cmdAction == 'Current') {
				$('#pageContent').load('../pages/register.html');
			} else if (cmdAction == 'Domiciliary') {
				$('#pageContent').load('../pages/register.html');
			} else if (cmdAction == 'Help me decide') {
				$('#pageContent').load('../pages/account_search_step1.html');
			} else if (cmdAction == 'Airtime') {
				$('#pageContent').load('../pages/mobilereload.html');
			} else if (cmdAction == 'Reset atm pin') {
				$('#pageContent').load('../pages/changepin.html');
			} else if (cmdAction == 'Feedback') {
				$('#pageContent').load('../pages/feedback.html');
			} else if (cmdAction == 'Request internet banking') {
				$('#pageContent').load('../pages/internetbanking.html');
			} else if (cmdAction == 'New atm card') {
				$('#pageContent').load('../pages/newcard.html');
			}
		}
	} else {
		console.log('Element id undefined');
	}
}

function closeNav() {
	document.getElementById("mySidenav").style.width = "0";
	document.getElementById('myModal').style.display = "none";
}

function closeByATMs() {
	document.getElementById("mySidenav").style.width = "100%";
	$('#pageContent').load('pages/atms.html');
}
